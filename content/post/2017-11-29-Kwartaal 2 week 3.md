---
title: Kwartaal 2 week 3
date: 2017-11-29
---
Vandaag is Maurits in de ochtend van de studiodag naar de teamcaptain meeting geweest.
Hij heeft ons erna verteld welke belangrijke dingen er in de meeting zijn besproken.
Samen met Quinty ben ik begonnen aan het opstellen van het testplan voor ons low-fid prototype.
We hebben tijdens het maken van het testplan feedback gevraagd aan een van de docenten.
We moesten specifieker zijn, en duidelijker naar voren brengen wat wij tijdens het testen
graag te weten willen komen. We hebben het testplan aangepast aan de hand van de feedback.
Vandaag hebben wij ook met elkaar een scrumboard gemaakt. We hadden al een online versie maar daar maakte
niemand echt gebruik van. Bob gaf als tip om een offline scrumboard te maken. Dit hebben wij dan ook gedaan.
In de middag zijn wij met het team verder gaan brainstormen over de drie concepten die wij eerder hadden bedacht.
We kwamen er helaas niet uit. We hebben meer informatie nodig over de doelgroep om goed te kunnen achterhalen
waar het probleem zit. 