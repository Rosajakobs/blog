---
title: Kwartaal 3 week 6
date: 2018-03-21
---
Vandaag was de tweede studiodag van week 6.
Na de workshop creatieve technieken, die Puck en ik de dag ervoor hadden gevolgd, hebben wij
het plan wat wij die dag ervoor hadden opgesteld uitgevoerd. Puck nam de leiding en we hebben
verschillende technieken met het team toegepast om op creatieve ideeën te komen voor het concept.
De technieken werkte erg goed, en we hadden er ook plezier in met het team.
Na de technieken zijn we gaan kijken welke dingen we konden meenemen voor het concept.
Na deze creatieve ochtend zijn Puck en ik naar de validatie van de Lifestyle diary gegaan (toekomstig.)
Deze werd gevalideerd door Micha. Helaas was deze niet gevalideerd. Wij hebben feedback gehad waar wij wat mee kunnen, en ik begreep de feedback.
Na de validatie zijn Puck en ik samen naar de Workshop ''Prototypen'' gegaan, deze werd gegeven door Bob. Na deze workshop was de studiodag afgelopen.
We hebben met het team afgesproken dat ieder voor zich een klein concept bedenkt met daarbij een low-fid prototype. Zo kunnen wij de concepten met elkaar vergelijken 
en maandag met elkaar bespreken, wat wij willen meenemen in het ontwerp. 