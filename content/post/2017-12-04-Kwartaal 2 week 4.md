---
title: Kwartaal 2 week 4
date: 2017-12-04
---
Vandaag stonden er twee validaties op het programma. Quinty is naar de validatie ontwerpcriteria
gegaan, en ik ben samen met Nuray voor de tweede keer naar de validatie Low-fid prototype gegaan.
Helaas heeft deze validatie veel tijd in beslag genomen. Samen met Nuray heb ik 1,5 uur moeten wachten
voordat wij daadwerkelijk aan de beurt waren. Helaas is ons low-fid prototype voor de tweede keer ook niet goedgekeurd.
Na deze validatie zijn wij weer opnieuw gaan brainstormen over het concept. Wij hebben hiervoor een negatieve brainstorm
gebruikt om te achterhalen wat wij vooral niet willen. Tijdens de brainstorm liepen wij een beetje vast en hebben wij
hulp van een peercoach gevraagd. Zij heeft ons geholpen met verschillende brainstorm technieken. 
Quinty is einde van de middag naar het oefen moment gegaan van de Pitch die zij gaat geven voor de opdrachtgever (Fabrique.)
