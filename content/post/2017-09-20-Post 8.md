---
title: Post 8!
date: 2017-09-20
---
Vandaag hebben we tijdens de studiodag verschillende spellen gespeeld.
Deze spellen waren van belang om te observeren en voor het maken van de  
spelanalyse. Het was leuk om de spellen te spelen en te observeren hoe de 
spelers zich gedragen tijdens  het spelen van een spel. In de middag heb ik 
samen met Joshua en Beau de workshop creatief mindmappen gevolgd van Pia.  
Deze workshop was erg zinvol  en leuk om te volgen. De dingen die ik daar heb
opgestoken zullen zeker nog van toepassing kunnen komen de komende weken.  