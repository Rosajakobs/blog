---
title: Kwartaal 2 week 4
date: 2017-12-06
---
Deze ochtend heeft Quinty de Pitch gepresenteerd aan docenten en de opdrachtgever.
We hebben positieve feedback gekregen over ons concept. Fabrique gaf aan dat hij het goed
vondt dat wij iets in Paard zelf hadden bedacht. Verder moeten wij wel nog goed na denken
over de verschillende functies op het apparaat. 's middags is Quinty naar het validatie moment geweest
van Ontwerpcriteria. Helaas was deze nog steeds niet helemaal goed gekeurd, en heeft zij nieuwe feedback ontvangen.
De rest van de dag zijn wij verder gegaan met het concept. 