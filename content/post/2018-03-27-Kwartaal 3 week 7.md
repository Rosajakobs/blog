---
title: Kwartaal 3 week 7
date: 2018-03-27
---
Vandaag hebben wij de laatste dingen van het project met elkaar besproken.
Dit was de laatste studiodag voor de competentie tafels. 
Ik ben naar aanleiding van het concept deskresearch gaan doen. Wij hadden over gezonde
of gezondere voeding nog niet veel informatie dus ben ik daar alsnog naar opzoek gegaan.
Puck en Jiska hebben zich bezig gehouden met het maken van een Persona.
We moesten de doelgroep beter inzichtelijk maken. Floris is begonnen aan de recap.
In de middag ben ik nog langs Robin geweest, om een van mijn STARRTS met hem te bespreken.
Dit vond ik erg fijn zo net voor de deadline van het leerdossier. 
Volgende week vinden de competentie tafels plaats en hebben wij dus geen studiodagen. 
Na de competentie tafels begint onze tweede sprint waarin wij ons verder verdiepen in de gezondheid
van de jongeren in Rotterdam-Zuid. 