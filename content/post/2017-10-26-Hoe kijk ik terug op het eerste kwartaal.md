---
title: Hoe kijk ik terug op het eerste kwartaal?
date: 2017-10-26
---
De afgelopen perioden zijn er heel veel nieuwe dingen gebeurd. Ik ben begonnen 
aan mijn opleiding CMD wat totaal nieuw voor mij was. Alles wat ik de afgelopen 
perioden heb gedaan of opgestoken was 90 procent nieuwe informatie. Deze nieuwe
informatie was in het begin erg onduidelijk, maar al snel heb je een heleboel dingen 
door. Het was belangrijk om continu gefocust te zijn tijdens de workshops, 
hoorcolleges en studiodagen. Ik heb geleerd om samen te werken met andere 
klasgenoten en dat is niet altijd even makkelijk, zeker wanneer dit de eerste 
keer is. Ik heb heel veel geleerd in deze eerste perioden. Alles wat ik in mij
heb opgenomen wil ik graag meenemen naar het tweede kwartaal. In mijn STARRT’s
is terug te lezen hoe bepaalde processen zijn verlopen en hoe ik deze de volgende
keer wil aanpakken. Ik heb de eerste design challenge naar tevredenheid afgesloten 
en ik ga met goede moed en veel enthousiasme het tweede kwartaal in. Ik hoop elk
kwartaal een stukje professioneler en vaardiger te worden. Mijn doel voor de komende 
periode is om zoveel mogelijk persoonlijke feedback te vragen waar kan. Door van een 
ander te horen wat jou verbeterpunten zijn kan je op de juiste manier werken aan
het doel dat je wilt bereiken. 