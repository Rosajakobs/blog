---
title: First post!
date: 2017-09-04
---
## 1e dag van de design challenge. 
Opgestart met het project, algemene uitleg gekregen over de opdracht 
Met elkaar aan de slag gegaan, afspraken en doelen van dit kwartaal met elkaar besproken. 
Na een goed beeld te hebben geschetst van wat wij met elkaar willen bereiken hebben wij een planning gemaakt en met elkaar de doelgroep van ons project bepaalt. 
Aan de hand van het bepalen van de doelgroep zijn we begonnen met het verdelen van verschillende onderzoeksvragen die wij verder die dag hebben uitgewerkt. 
Tussen de design challenge door moesten wij een poster maken met daarin onze kwaliteiten, afspraken en doelen die we met elkaar hadden besproken. 
Aan het einde van de middag hebben we met elkaar besproken wanneer we bepaalde dingen af wilde hebben en wat we erna willen gaan aanpakken.   