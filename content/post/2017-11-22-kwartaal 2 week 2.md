---
title: Kwartaal 2 week 2
date: 2017-11-22
---
Vandaag was de tweede studiodag van week twee. We hebben verschillende dingen
waar we eerder al aan waren begonnen af kunnen maken. In de ochtend heb ik samen met 
Maurits een online scrumboard gemaakt. Dit scrumboard is toegankelijk voor alle teamleden
en kan zo dus ook bekeken worden op momenten dat we niet op school aanwezig zijn.
Daarnaast kan iedereen hierin dingen aanpassen en veranderen. Ik heb mij bezig gehouden
met mijn merkanalyse over het Ziggo Dome. In de middag ben ik samen met Quinty naar de 
workshop SCRUMBOARD geweest. Deze workshop werd gegeven door Bob. Ik was al bekend
met deze methode omdat wij hier met het vorige team ook gebruik van hebben gemaakt.
Toch vond ik het nuttig om hier extra informatie over te krijgen, omdat er veel verschillende
mogelijkheden zijn binnen het scrumboard. Na de workshop hebben wij met het hele team
de deliverables voor week twee afgemaakt en geupload. In ons online scrumboard hebben wij
een aantal dingen toegevoegd waar we ons buiten de studiodagen alvast mee bezig kunnen houden.
Zo hebben wij ons alvast een beetje voorbereid op de deliverables van week drie.  