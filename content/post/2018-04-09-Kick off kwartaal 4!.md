---
title: Kick off kwartaal 4!
date: 2018-04-09
---
Vandaag zijn we gestart met het nieuwe kwartaal. Kwartaal 4 het laatste kwartaal van dit jaar.
Dit kwartaal werken wij verder in dezelfde teams aan dezelfde opdracht. In de ochtend hebben wij een presentatie
gehad van opdrachtgever Joke. Zij gaf informatie over het onderwerp waar wij ons nu op moeten richten. Dit gaat om de gezondheid
van de jongeren in Rotterdam-Zuid. De eerste stap om verder te gaan is om hierbij een wijk uit te kiezen in Rotterdam-Zuid waar we het onderzoek en het ontwerp op willen
richten. Na de presentatie zijn wij direct aan de slag gegaan. We zijn eerst gaan kijken wie aan welke competenties moet gaan werken dit kwartaal en daarbij
de juiste deliverbales geplaatst. Dit wordt uitgewerkt in een uitgebreide planning in het PVA.