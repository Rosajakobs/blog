---
title: Kwartaal 2 week 7
date: 2018-01-10
---
De dag begon met de teamcaptain meeting. Maurits is naar deze meeting geweest.
Hij heeft ons erna vertelt wat er tijdens de meeting besproken is.
We zijn begonnen met het voorbereiden van de expo. Quinty en ik zijn samen begonnen
aan het maken van een poster. Nuray is begonnen aan het hihg-fid prototype. In de middag
zijn Quinty en ik naar het validatie moment van de Designrationale geweest. Helaas
is deze nog onvoldoende. Wij kregen als feedback, dat de Designrationale niet overeen komt
met ons concept. Hij gaf als tip om het concept aan te passen en nog eens goed te kijken naar het probleem.
Wij baalde hier enorm van omdat de expo al over een week is. Toch wilde wij verder kijken omdat wij de feedback
begrepen en zelf ook niet 100% achter het concept stonden. We hebben een alumni erbij gehaald zodat hij ons verder kon helpen.
Quinty en ik hebben die middag nog een aanpassing gemaakt met de ideeën die wij al hadden waardoor wij toch een heel ander concept
kunnen creeëren. Helaas was de rest van ons team al naar huis, dus hebben wij hun op de hoogte gehouden over het nieuwe concept via de app.
Ze waren het eens met het nieuwe idee, en pakken hier dus mee door in ons high-fid prototype voor de expo. 