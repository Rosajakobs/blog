---
title: Kwartaal 3 week 7
date: 2018-03-26
---
In de ochtend begonnen we met het bij elkaar voegen van de concepten die wij individueel hadden bedacht. We hebben gekeken of de ideeën haarbaal en/of uitvoerbaar zijn. Toen we een beetje vast liepen met bepaalde dingen heb ik Bob gevraagd om ons te helpen. Hij zei dat het belangrijk is om bepaalde keuzes te maken, en goed te weten waarom je voor een bepaalde doelgroep ontwerpt. Jiska en Puck zijn begonnen met het maken van een Persona om zo een beter beeld te krijgen van de doelgroep. In de middag ben ik met Puck naar Mieke gegaan om ons verslag te laten valideren van de creatieve sessie. Deze was voldoende gevalideerd.  