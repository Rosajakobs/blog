---
title: Kick off kwartaal 3
date: 2018-02-06
---
Vandaag hebben wij de kick off van het nieuwe project gehad. We kregen een korte presentatie van de opdrachtgever. Deze periode zal 15 weken duren, en opgesplitst worden in twee delen. De eerste 8 weken houden wij ons bezig met de gezondheid van de jongeren op de Hogesc
hool Rotterdam. In de tweede fase verdiepen wij ons in de gezondheid van de jongeren in Rotterdam Zuid.  Verder hebben wij uitleg gekregen over de Maastrichtreis.  