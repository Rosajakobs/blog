---
title: Maastrichtreis
date: 2018-02-07
---
De eerste week van ons nieuwe project zijn we naar Maastricht geweest.
Deze reis stond in het teken van ons nieuwe project. De eerste dag werden er teams gevormd
met de andere CMD klassen uit het eerste leerjaar. We hebben die middag een doelgroep toegewezen
gekregen waarvoor we moesten ontwerpen. Met het team zijn we Maastricht in gegaan om 50 plussers te intervieuwen over hun gezonde leefstijl.
We hebben gevraagd naar beweging voeding en ontspanning. We hebben een paar leuke gesprekken gehad op straat.
Na het intervieuwen hebben we een persona gemaakt die onze doelgroep goed weergeeft. In de avond was er een barbeque en erna ben ik met de klas opstap gegaan.
De tweede dag, moesten wij een concept bedenken om de 50 plusser te stimuleren tot een gezondere levensstijl. Dit concept werkte je uit in een klein prototype
dat op de expo kon worden gepresenteerd. Uiteindelijk zijn wij op het idee gekomen om een koffieautomaat te verzinnen waarvoor je moet lopen op een trap om koffie te
krijgen. De trap zit als het ware vast aan de koffieautomaat. Denk aan een fitnes apparaat in de sportschool. De 50 plusser is toch regelmatig op het werk te vinden
en daarbij komt vaak koffie kijken. Koffie is niet het meest gezonde product en zeker niet wanneer er ook melk en suiker in zit. Daarnaast beweeg je weinig op kantoor.
Heeft de 50 plusser geen zin om moeite te doen voor zijn koffie? Dan kan er zonder moeite thee worden getapt, dit is beschikbaar zonder op de trap te lopen omdat thee
een stuk gezonder is voor de mens. Bij ons concept hadden we de koffieautomaat in het klein nagebouwd. Einde van de middag werden de concepten aan elkaar gepresenteerd op
de expo. Per doelgroep werd er een winnaar gekozen. In de avond ben ik met een paar klasgenoten uiteten geweest en erna zijn wij opstap geweest. De volgende dag zijn
wij weer vertrokken naar huis. 