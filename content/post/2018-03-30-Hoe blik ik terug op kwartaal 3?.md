---
title: Hoe blik ik terug op kwartaal 3?
date: 2018-03-30
---
Het derde kwartaal is alweer bijna voorbij. Ik vond dit een erg lastig kwartaal
omdat wij erg zelfstandig moesten werken. Het was de eerste keer dat wij een
eigen Plan van aanpak moesten opstellen. Dit vond ik erg lastig. Doordat wij hier in het
begin van het kwartaal veel tijd aan hebben besteed zijn wij moeilijk op gang gekomen.
Ik vond het lastig om eigen methodes te kiezen die waardevol zijn voor het proces. 
De volgende keer zou ik hier graag wat meer begeleiding bij willen van een docent.
Dit kan ik natuurlijk zelf aangeven. Ik vond het team waarin ik zat fijn om mee samen
te werken. Wel vond ik dat er af en toe weinig gebeurde en dat iedereen een beetje zijn
eigen ding deed. Zelf heb ik hier ook niet heel veel aan gedaan. De laatste twee weken
zag ik wel een stijgende lijn, en deed iedereen zijn ding. Misschien kwam dit omdat wij
toen eindelijk pas echt op gang kwamen en de druk van de deadlines ook wat hoger werd. 
Ik denk dat ik van deze sprint veel geleerd heb dat ik kan verbeteren in de aankomende weken.
Ik weet nu dat een Plan van aanpak heel belangrijk is om goed van start te gaan. Ik zie de volgende weken wel positief
in omdat ik ondanks dat deze sprint moeizaam ging toch dingen heb geleerd. Nu weet ik welke dingen ik de volgende
weken anders wil aanpakken. 