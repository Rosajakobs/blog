---
title: kwartaal 2 week 7
date: 2018-01-08
---
De eerste studiodag na de vakantie is aangebroken. We begonnen de dag met studiecoaching 
van Sylvia. Deze les duurde ongeveer tot 11 uur. In de middag ging Quinty naar de workshop
Designrationale. Ik heb deze dag helaas niet veel aan het project kunnen werken. In de middag
had ik de tekentoets waarvoor ik in kwartaal 1 was gezakt. Deze toets duurde ongeveer drie uur.