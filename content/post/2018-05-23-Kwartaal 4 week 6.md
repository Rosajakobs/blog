---
title: Kwartaal 4 week 6
date: 2018-05-23
---
In de ochtend hebben Puck en ik feedback gevraagd op onze wireframe schetsen.
Met de feedback die Robin ons had gegeven konden wij starten met het High-fid prototype.
Voor dat wij begonnen aan het High-fid prototype hebben wij een styleguide gemaakt.
We hebben gekeken welke kleuren en lettertypes wij terug willen laten komen in het gehele ontwerp.
In de middag zijn Puck en ik begonnen aan het uitwerken van het prototype. We begonnen met het maken
van een logo, daarna zijn wij begonnen met het uitwerken van het prototype in Adobe XD. Het prototype moet volgende week worden afgemaakt. 