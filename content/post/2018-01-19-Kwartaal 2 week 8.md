---
title: Kwartaal 2 week 8
date: 2018-01-19
---
Vandaag is de laatste dag dat wij aan het project konden werken. In de avond
moesten wij ons project presenteren voor, docenten, de opdrachtgever en andere gasten
die uitgenodigd waren op de expo. De expo begon half 7 's avonds. Overdag hebben wij nog
het een en ander afgemaakt voor de presentatie van de expo. Quinty, Nuray en ik hebben een poster
gemaakt. Aaron heeft aangeboden om voor de expo de pitch te maken en te presenteren. 
Tijdens de Pitch hebben we feedback gekregen van Robin. Hij heeft de pitch en het concept
gevalideerd. De feedback die hij gaf was ik het mee eens en zal ik zeker mee nemen naar het
volgende kwartaal. Ik vond het leuk om tijdens de expo ook bij andere klassen te kijken. Hierbij
zie je hoeveel verschil er kan zitten tussen bepaalde klassen. De expo liep rond 9 uur tot op zijn eind. 