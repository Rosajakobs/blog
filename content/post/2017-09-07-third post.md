---
title: Third post!
date: 2017-09-07
---
Vandaag hebben we een werkcollege gevolgd over het toepassen van de theorie 
die we in het hoorcollege hebben gevolgd. Dit ging voornamelijk over het 
toepassen van het ontwerpproces en dat je dit proces bij heel veel 
verschillende dingen die je doet of moet voorbereiden kan laten terug komen. 
Dit proces is van belang bij alle design opdrachten de komende jaren.  
Ik vond het een goed en duidelijk hoorcollege en zal de dingen die we hebben 
geleerd en gedaan nog vaak toepassen in bij andere opdrachten. 