---
title: Kwartaal 2 week 1
date: 2017-11-16
---
Vandaag hebben we de nieuwe teams gevormd voor het tweede kwartaal. We moesten de 
teams samenstellen aan de hand van jouw belbinrol. Hierdoor werden er teams
gevormd waarin iedereen een andere kwaliteit bezit. Ons team heeft vier studenten
van 1E en een student van 1D. We waren tevreden met het team en zijn begonnen
met de teaminventarisatie. We hebben een teamnaam bedacht en Nuray heeft daarbij een 
passend logo gemaakt. We hebben de planning in natschool doorgenomen zodat we een goed
inzicht hadden over wat ons deze periode staat te wachten. Verder hebben we gewerkt
aan de Brief en zijn sommige alvast begonnen aan de merkanalyse. 