---
title: Kwartaal 3 week 5
date: 2018-03-13
---
Vandaag ben ik naar de workshop datavisualisatie geweest. Hierbij hebben wij verschillende inzichten gekregen, hoe wij data inzichtelijk kunnen maken op een visuele manier. Samen met Puck heb ik die les besteed aan het analyseren van ons onderzoek wat wij die dag ervoor hebben gedaan. 
De informatie die wij uit ons onderzoek hebben gehaald, willen wij ook nog visueel gaan maken, door middel van de voorbeelden die werden gegeven in de workshop 