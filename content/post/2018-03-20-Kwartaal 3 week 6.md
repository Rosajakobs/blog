---
title: Kwartaal 3 week 6
date: 2018-03-20
---
Vandaag heb ik de workshop Creatieve technieken gevolgd, gegeven door Mieke.
Deze workshop was erg nuttig. We kregen inzicht in verschillende creatieve technieken, en de bedoeling
was om deze ook te gaan toepassen binnen je team. Tijdens de workshop moest je een soort PVA maken
waarin je verschillende creatieve technieken moest opstellen die jij nuttig vond voor het proces.
Je stelde op wanneer je deze wilde uitvoeren en wat je er mee wilde bereiken. Ik heb dit samen met Puck gedaan en willen
het ook graag in de tweede studiodag van deze week gaan uitvoeren. Het gemaakte PVA van deze workshop kan de week erna
ook gevalideerd worden. Door het uitvoeren van de technieken kan je reflecteren wat dit voor jou en je team heeft opgeleverd.
Deze uitwerking kan meegenomen worden naar deel twee van de workshop en dit kan worden gevalideerd. Dat vindt ik erg fijn zodat
ik dit kan opnemen in mijn leerdossier. Einde van de middag ben ik nog naar mijn keuzevak gegaan. (Website bouwen voor personal branding.)