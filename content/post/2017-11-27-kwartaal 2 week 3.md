---
title: kwartaal 2 week 3
date: 2017-11-27
---
Week drie is begonnen, en de maandag betekend nieuwe deliverbales voor deze week.
Quinty is deze ochtend naar de teamcaptain meeting geweest, en heeft ons op de hoogte gebracht
over wat er allemaal besproken is. We zijn begonnen met het brainstormen over verschillende concepten.
We hebben onze ideeën op papier gezet en zijn aan de hand daarvan begonnen aan het uitwerken
van een low-fid prototype. Samen met Quinty heb ik de ontwerpcriteria opgesteld.
Wij hadden hier nog verschillende vraagtekens bij en zijn daarmee naar Bob gegaan voor feedback.
De feedback was waardevol, waardoor wij de opdracht hebben kunnen afmaken. 
