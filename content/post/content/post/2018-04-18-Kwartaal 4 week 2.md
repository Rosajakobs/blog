---
title: Kwartaal 4 week 2
date: 2018-04-18
---
Vandaag zijn we met het hele team naar onze gekozen wijk gegaan in Rotterdam-Zuid (Afrikaanderwijk)
We zijn gaan observeren in de omgeving om te kijken wat de wijk te bieden heeft. 
We hebben veel verschillende restaurants gezien die gericht zijn op de cultuur in Afrikaanderwijk.
We hebben rond gelopen op de weekelijkse markt die daar word gehouden om te kijken of we onze doelgroep daar konden vinden. 
Helaas was dit niet het geval. Wel hebben we hierdoor de sfeer van Afrikaanderwijk kunnen observeren.
Als laatste zijn we in de Jumbo geweest die vlak naast een middelbareschool gevestigd is. Hier hebben wij geobserveerd
wat de leerlingen voor producten kopen. Het meest opvallend was dat de leerlingen ongezeonde producten kochten.
In de middag heb ik de workshop usability testing gevolgd van Fedde. 