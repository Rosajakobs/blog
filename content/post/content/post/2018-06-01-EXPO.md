---
title: EXPO
date: 2018-06-01
---
Vandaag was het alweer de allerlaatste expo van jaar 1.
Om de laatste puntjes op de i te zetten ben ik vandaag optijd naar school gegaan.
We hebben de allerlaatste dingen afgemaakt en de expo opgebouwd. De opdrachtgever,
docenten en medestudenten liepen langs om ons gemaakte werk te bewonderen. Tijdens de expo
is onze poster en prototype gevalideerd door Bob en Elske. Ik heb nog wat laatste feedback
kunnen vragen om dit op te nemen in het leerdossier. De volgende keer wil ik graag nog wat extra
werken aan de presentatie van de expo. Ik heb de afgelopen weken zo gefocust op het project zelf
waardoor de expo mij een beetje is ontgaan. De eindpresentatie is een erg belangrijk onderdeel om 
proffessioneel over te komen bij de opdrachtgever. Ik hoop dit kwartaal positief te kunnen afsluiten door
tijdens het assesment mijn punten binnen te halen. Ik heb erg hard gewerkt het laatste kwartaal en ben 
tevreden met het concept dat wij vandaag hebben gepresenteerd. 