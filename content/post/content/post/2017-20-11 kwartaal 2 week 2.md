---
title: Kwartaal 2 week 2
date: 2017-11-20
---
Vandaag was de eerste dag dat we zijn begonnen met het werken aan onze deliverables.
In de eerste les zijn we er al een klein beetje mee opgestart maar zijn we er nog niet
erg ver mee gekomen. Ik heb deze dag gemerkt dat iedereen nog een beetje los moet komen
en dat we moeten wennen aan elkaar. Ik vond het soms lastig om te pijlen waar iedereen mee
bezig was. Iedereen was nog een beetje in de afwachtende houding. We hebben allemaal individueel
aan de merkanalyse gewerkt. We hebben er voor gekozen om allemaal een andere locatie
te onderzoeken. Hierdoor kun je achteraf informatie met elkaar vergelijken. We zijn deze dag
ook begonnen met het opstellen van enquete vragen waarmee we meer informatie te weten kunnen
komen over onze doelgroep. Einde van de studiodag hebben we een SWOT analyse en samenwerkingscontract opgestelt.

