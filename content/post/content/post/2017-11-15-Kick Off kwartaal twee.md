---
title: Kick off kwartaal twee
date: 2017-11-15
---
Vandaag hebben wij de kick off van het tweede project gehad. Deze kick off
vond plaats in het PAARD in Denhaag. Deze locatie had natuurlijk te maken met het
project waar we deze periode mee aan de slag gaan. We hebben uitleg gekregen over
het nieuwe kwartaal, en aansluitend op de opdracht een film gekeken. Ik vond het
leuk dat we op deze locatie hadden afgesproken, Hierdoor heb je gelijk een eerste
indruk en gevoel bij de locatie. Ik was enthousiast na het horen van de opdracht 
en was erg nieuwschierig naar de nieuwe teamvorming. 