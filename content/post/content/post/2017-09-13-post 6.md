---
title: Post 6!
date: 2017-09-13
---
In de ochtend hebben we het paperprototype aan elkaar gepresenteerd, het was 
leuk om de ideeën van andere te kunnen bekijken en feed back aan elkaar te geven.  
Voor een eerste paperprototype hebben wij zeer positieve feedback ontvangen. 
Het spel was duidelijk en de uitleg was makkelijk te volgen via het paperprototype.  
In de middag hebben we met vier mensen ons spel getest. Een persoon van ons groepje 
ging naar de workshop moodboard maken. We zijn naar buiten gegaan en hebben een aantal 
locaties bezocht. Tijdens het spel hebben we foto's gemaakt om het spelverloop voor 
andere ook inzichtelijk te maken. Na het spelen van het spel zijn we begonnen met de 
spelanalyse. Hieruit zijn een aantal conclusies gekomen die we mee willen nemen naar 
het volgende prototype.  