---
title: Kwartaal 4 week 1
date: 2018-04-11
---
Vandaag ben ik begonnen met het uitwerken van het PVA. Omdat ik de competentie
inrichten ontwerpproces nog moet behalen wil ik graag het PVA maken. Afgelopen maandag
hebben wij gekeken naar welke competenties iedereen nog moet behalen. Vandaag heb ik deze verwerkt
in een uitgebreide planning. Puck had van te voren een overzicht met technieken opgesteld
deze heb ik ook verwerkt in de planning. De planning is een duidelijk overzicht met daarin
deliverables, technieken verschillende fases en de teamnamen staan vernoemd achter elke taak. 
Naast de planning heb ik ook de voorkant van het PVA vormgegeven. Hierop staat de vraag uit de praktijk geformulierd zodat in een oogopslag
duidelijk is waar wij ons op richten bij dit project. Het team staat duidelijk vermeld en ik laat zien dat wij ons dit kwartaal op voeding richten.
Ook staat er vermeld wie welke competenties nog moet behalen. We hebben het PVA uitgeprint en opgehangen in de studio. In de middag is deze voldoende gevalideerd
door Robin. 