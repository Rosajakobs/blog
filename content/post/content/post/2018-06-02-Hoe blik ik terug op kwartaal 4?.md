---
title: Hoe blik ik terug op kwartaal 4?
date: 2018-06-02
---
Het eerste jaar van CMD zit er alweer bijna op. Het laatste kwartaal heb ik als
een van de beste/leukste kwartalen ervaren. In de vorige kwartalen verliep alles vrij chaotisch
ten opzichte van het laatste kwartaal. Ik had dit kwartaal alles veel beter op een rijtje en wist
precies wat ik moest doen. De communicatie binnen het team was erg sterk en we hielden elkaar op de hoogte
en/of hielpen elkaar waar nodig. De manier waarop ik het laatste kwartaal heb aangepakt heeft voor mij het beste gewerkt.
Ik heb veel beter gepland, en het plan van aanpak dat ik zelf had opgesteld was voor mij een goede houvast. 
Ik heb het eerstejaar CMD enorm veel geleerd en elk kwartaal waren er dingen die beter gingen. Ondanks dat ik in het
begin van het jaar erg moeilijk inkwam ben ik nu zeker van mijn zaak dat ik verder wil met deze opleiding. Ik ben erg
nieuwschierig wat mij het tweede jaar CMD allemaal te wachten staat. 