---
title: Kwartaal 4 week 5
date: 2018-05-16
---
In de ochtend zijn Puck en ik begonnen aan het schetsen van de wireframes. 
Het concept dat wij bedacht hebben bevat geen interactief product. Omdat Puck en ik nog
aan de competentie verbeelden en uitwerken moesten werken hebben wij ter versterking van het concept
een app en website aan het concept toegevoegd. Ons concept draait dus niet om de app of website maar hier 
is wel het een en ander te vinden over het concept en de app heeft daadwerkelijk een toegevoegde waarde.
Ik ben begonnen met het schetsen van de wireframes van de app. Puck heeft deze van de website gemaakt.
Ik heb mij vooral gericht op het design. Wat komt waar te staan en welke onderdelen willen wij verwerken in de app.
Ik heb deze wireframes geschetst in een papieren iphone model. Daarna heb ik deze uitgeknipt en opgehangen in de studio. 
In de middag ben ik naar twee workshops geweest. Om 13:00 naar de workshop empathie van Bob en daarna naar de workshop
Designrationale van Robin. Deze workshops waren erg nuttig. 