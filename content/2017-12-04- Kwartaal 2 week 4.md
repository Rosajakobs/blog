---
title: Kwartaal 2 week 4
date: 2017-12-04
---
De week is weer begonnen, we zijn inmiddels alweer in week vier van kwartaal 2.
Vandaag stonden er twee validaties op de planning. Quinty is naar de validatie ontwerpcriteria gegaan
en Nuray en ik naar de validatie low-fid prototype van Mio. Helaas ging de validatie van de ontwerpcriteria niet door.
Quinty heeft hier wel algemene uitleg over gehad en de validatie is verplaatst naar woensdg week 4.
Nuray heeft bij de validatie van het low-fid prototype de drie ideeën uitgelegd (concepten) die wij op dit moment
bedacht hadden. Helaas was dit niet voldoende om het product te laten valideren voor het leerdossier. Het kwam er op neer
dat wij meer toe moeten werken naar een compleet concept. De ideeën die we nu hadden waren meer marketing adviezen.
Wij hebben na het validatie moment opnieuw gekeken naar onze ideeën. We hebben een negatieve brainstorm gehouden om erachter te komen
wat wij absoluut niet wilde. Einde van de middag is Quinty naar de oefen ronde van de pitch gegaan.