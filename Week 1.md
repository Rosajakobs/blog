---
title: Week 1 kwartaal 4
date: 2018-04-11
---
Vandaag zijn Puck en ik bezig geweest met het uitwerken van het PVA. Puck had ter voorbereiding een overzicht gemaakt met methoden
die wij per fase van het ontwerpproces kunnen uitvoeren. Ik heb een planning opgesteld met daarin alle deliverbales, methoden en technieken die wij willen toepassen.
Deze planning is onderverdeelt in Ideation, inspiration en implementation. De deliverbales zijn gebaseerd op de competenties die nog per persoon behaald moeten worden,
dus bij een deliverable staat vermeld wie hier dit kwartaal aan werkt en hoelang. Zo hebben wij een duidelijk overzicht wie waar aan moet werken in deze laatste periode.
Naast de planning en methoden en technieken hebben ik ook een a3 blad vormgegeven met daarop de belangrijkste dingen waar wij ons dit kwartaal op richten. Zo hebben
wij een overzicht waar wij ons op moeten focussen en wat belangrijke aspecten zijn die terug te zien zijn in het eindresultaat. Einde van de middag hebben Puck en ik
het PVA kunnen laten valideren bij Robin, deze was voldoende. Nu hangt hij aan de wand bij de studio zodat wij een goede houvast hebben bij het project.